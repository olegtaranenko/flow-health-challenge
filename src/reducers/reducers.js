const cleanBoard = [ [ 1, 2, 3 ], [ 4, 5, 6 ], [ 7, 8, 9 ] ];

const initialState = {
  // different properties from api, mainly from /api/game and /api/move

  // current state of the game
  board: cleanBoard,
  // which team plays player - 'X' or 'O'
  player: '',
  // which team plays ai - 'X' or 'O'
  ai: '',
  // who should do move ('X' or 'O')
  nextMove: '',

  // last Player move's cell index, started with 1.
  // 1 is upper left corner, which correspond to what getting from backend
  // this variable is set only after current move. After app reload it will disappeared
  index: undefined,

  // last AI move's cell index, started with 0.
  // 1 is upper left corner, which correspond to what getting from backend
  // this variable is set only after current move. After app reload it will disappeared
  aiIndex: undefined,
  // who is the winner of the game 'ai' or 'player'
  winner: undefined,
  // winning team 'O' or 'X'
  team: '',
  // the game is finished
  end: undefined,
  // current game history. It contains only made moves
  // after app refresh it will be clean
  history: [],

  // all games log, getting by /api/score request
  scores: [],

  // technical variables to visual representation of backend activity
  // show loading wheel?
  loading: true,
  // last request to backend has troubles
  failed: false,
  failedScope: '',
};

function detectAiIndex(xOrO, board, prevBoard) {
  let index = 0;
  let found = false;
  let foundIndex = undefined;
  board.some((row, rowIndex) => {
    const prevRow = prevBoard[rowIndex];
    row.some((cell, colIndex) => {
      ++index;
      found = cell !== xOrO || cell === prevRow[colIndex];
      if ( !found ) {
        foundIndex = index;
      }
      return !found;
    });
    return !found;
  });
  return foundIndex;
}

// restore the history only in assumption, that we are waiting player's move
// This is just guessing and may not match to real game flow.
// Then if the board is almost full, highly likely it differs.
const restoreHistory = (board, player) => {
  const history = [];
  if ( player === 'O' ) {
    history.push(0);
  }
  let index = 0;
  const playerMoves = [], aiMoves = [];
  board.forEach((row, rowIndex) => {
    row.forEach((cell, colIndex) => {
      ++index;
      if ( typeof cell !== 'number' ) {
        if ( cell === player ) {
          playerMoves.push(index);
        } else {
          aiMoves.push(index);
        }
      }
    })
  });

  for ( let i = 0; i < aiMoves.length; i++ ) {
    const playerMove = playerMoves[i];
    const aiMove = aiMoves[i];
    if ( player === 'X' ) {
      history.push(playerMove, aiMove);
    } else {
      history.push(aiMove);
      if ( playerMove) {
        history.push(playerMove);
      }
    }
  }

  return history;
};

const reducer = (state = initialState, action) => {
  switch ( action.type ) {

    case 'MOVE_REQUESTED': {
      const { index } = action;
      const { history } = state;
      const newHistory = history.slice();
      newHistory.push(index);

      return {
        ...state,
        index,
        history: newHistory,
        loading: true,
        failed: false
      };
    }

    case 'MOVE_PROCEEDED': {
      const { result } = action.payload;
      let prevBoard = [], prevHistory;
      {
        const { board, history } = state;
        prevBoard = board;
        prevHistory = history;
      }
      const { board, ai, end, winner, team} = result;
      const aiIndex = detectAiIndex(ai, board, prevBoard);
      let history = prevHistory;
      if ( aiIndex ) {
        history = prevHistory.slice();
        history.push(aiIndex)
      }

      let { scores } = state;
      if (end) {
        scores = scores.slice();
        const ts = new Date().getTime();
        scores.push({ts, winner, team})
      }
      return {
        ...state,
        ...result,
        aiIndex,
        history,
        scores,
        loading: false,
        failed: false
      };
    }

    case 'NEXT_GAME': {
      const { result } = action.payload;
      const { ai, board, player } = result;
      const aiIndex = detectAiIndex(ai, board, cleanBoard);
      const history = restoreHistory(board, player);
      return {
        ...state,
        ...result,
        board,
        aiIndex,
        history,
        // drop the flags in state
        end: false,
        winner: '',
        team: '',
        loading: false,
        failed: false
      };
    }

    case 'GAME_REQUESTED':
      return {
        ...state,
        loading: true,
        failed: false
      };

    case 'GAME_LOADED': {
      const { result } = action.payload;
      const { board, player } = result;
      const history = restoreHistory(board, player);
      return {
        ...state,
        ...result,
        history,
        loading: false,
        failed: false
      };
    }

    case 'SCORES_LOADED': {
      const { result: {list} } = action.payload;
      return {
        ...state,
        scores: list,
        loading: false,
        failed: false
      };
    }

    case 'GAME_QUERY_FAILED':
      const failed = action.failed === undefined ? true : !!action.failed;
      return {
        ...state,
        loading: false,
        failed: failed
      };

    default:
      return state;
  }
};

export default reducer;
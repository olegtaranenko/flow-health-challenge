import './style.scss';
import React, { Component } from "react";
import { connect } from 'react-redux';
import Moment from 'react-moment';

import { Table } from 'semantic-ui-react'

import { WithGameService } from "../../services/game-service";
import * as actions from "../../actions";

class ScorePage extends Component {

  componentDidMount() {
    const { GameService, scoresLoaded, gameServiceFailed } = this.props;
    GameService.getScores()
    .then(res => {
      scoresLoaded(res)
    })
    .catch(err => {
      return gameServiceFailed(err, 'getScores')
    })
  }

  render() {
    const {scores} = this.props;
    let teamGuess = '';

    return (
      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Game Time</Table.HeaderCell>
            <Table.HeaderCell>Team X</Table.HeaderCell>
            <Table.HeaderCell>Team O</Table.HeaderCell>
            <Table.HeaderCell>Result</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>{
          scores.map(({team, ts, winner}, key) => {
            const result = !winner ? 'Draw' : `${winner.toUpperCase()} Won`;
            const teamX = winner ? (team === 'X' ? 'AI' : 'PLAYER') : '-';
            const teamO = winner ? (team === 'O' ? 'AI' : 'PLAYER') : '-';
            return (
              <Table.Row key={key}>
                <Table.Cell>
                  <Moment format="D MMM HH:mm:ss" withTitle>{ts}</Moment>
                </Table.Cell>
                <Table.Cell>{teamX}</Table.Cell>
                <Table.Cell>{teamO}</Table.Cell>
                <Table.Cell>{result}</Table.Cell>
              </Table.Row>
            )
          })
        }
        </Table.Body>

      </Table>
    )
  }

}

const mapStateToProps = ({ scores }) => {
  return {
    scores
  }
};


export default WithGameService()(connect(mapStateToProps, actions)(ScorePage));

import './style.scss';
import React, { Fragment } from 'react';
import { Row, Col } from 'reactstrap';
import { connect } from 'react-redux';

import Board from "../../components/board";
import store from '../../store';
import { WithGameService } from "../../services/game-service";
import * as actions from "../../actions";

const PlayPage = () => {
  const state = store.getState();
  const {ai, player, end, history, winner} = state;
  const logEntry = (
    <div className='entry'>
      {history.map((cell, key) => {
        const move = cell === 0 ? '' : (cell === undefined ? '...' : cell + '');
        return (
          <div className='move' key={key}>{move}</div>
        )
      })}
    </div>
  );

  const teamWinner = !winner ? 'Draw!' : `Team ${winner.toUpperCase()} won!`;
  const info = end ? (
    <div className='entry info'>
      <div className='info'>{teamWinner}</div>
    </div>
  ) : null;
  return (
    <Fragment>
      <Row>
        <Col xs={12} lg={8} className='board-wrapper'>
          <Board/>
        </Col>
        <Col xs={12} lg={4} className='log-wrapper'>
          {/*<h3>Moves</h3>*/}
          <div className='entry'>
            <div className='header'>{`Player (${player})`}</div>
            <div className='header'>{`AI (${ai})`}</div>
          </div>
          {logEntry}
          {info}
        </Col>
      </Row>
    </Fragment>
  );
};


const mapStateToProps = ({ index, aiIndex, history, ai, player, nextMove, end, winner }) => {
  return {
    history,
    aiIndex,
    index,
    ai,
    player,
    end,
    winner,
  }
};

export default WithGameService()(connect(mapStateToProps, actions)(PlayPage));

import React from "react";
import './style.scss';

export default function Hero() {
  return (
    <div className='hero'>
      <h1>
        Tic-tac-toe Game donating to Flow Health
      </h1>
    </div>
  )
}

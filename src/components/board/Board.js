import React, { Component } from "react";
import './style.scss';

import { connect } from 'react-redux';
import * as actions from "../../actions";
import { WithGameService } from "../../services/game-service";

const axis = [ '', '', '' ];

class Board extends Component {

  componentDidMount() {
    const { GameService, gameLoaded, gameServiceFailed } = this.props;
    // gameRequested();
    GameService.getCurrentGame()
    .then(res => {
      gameLoaded(res)
    })
    .catch(err => {
      return gameServiceFailed(err, 'getCurrentGame')
    })
  }

  onSquareClick(cell, isEmpty) {
    const { GameService, gameServiceFailed, moveRequested, moveProceeded, end, nextGame } = this.props;
    if ( end ) {
      GameService.nextGame()
      .then((res) => {
        nextGame(res);
      })
      .catch((err) => {
        return gameServiceFailed(err, 'nextGame')
      });
    } else if ( isEmpty ) {
      moveRequested(cell);
      GameService.doMove(cell).then((res) => {
        moveProceeded(res, cell);
      })
      .catch((err) => {
        return gameServiceFailed(err, 'moveRequested')
      });
    }
  };


  render() {
    let cellIndex = 0;
    const { board } = this.props;
    let body = null;

    if ( board.length ) {
      body = axis.map((row, rowId) => {
        const vertical = rowId === 0 ? 'top' : ( rowId === 2 ? 'bottom' : '' );
        return (

          axis.map((col, colId) => {
            const horizontal = colId === 0 ? 'left' : ( colId === 2 ? 'right' : '' );
            let cell = board[rowId][colId];

            // if cell is number - it is empty for next possible move
            const isEmpty = typeof cell === "number";
            let cellClassName = '';
            if ( !isEmpty ) {
              cellClassName = cell.toLowerCase();
            }
            const positionClassName = `square ${vertical} ${horizontal}`.trim();

            return (
              <div key={`cell-${cellIndex++}`} className={positionClassName} onClick={
                () => {
                  this.onSquareClick(cell, isEmpty)
                }
              }>
                <div className={cellClassName}/>
              </div>
            )
          })
        )
      });
    }
    return (
      <div className="game">
        <div className="board">
          {body}
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ index, board, ai, player, nextMove, end, winner }) => {
  return {
    index,
    board,
    ai,
    player,
    nextMove,
    end,
    winner,
  }
};


export default WithGameService()(connect(mapStateToProps, actions)(Board));

import React from 'react';
import ReactDOM from 'react-dom';
// import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';

import App from './App';
import {GameServiceContext, GameService} from "./services/game-service";
import store from './store'

const gameService = new GameService();

ReactDOM.render(
  <Provider store={store}>
    <GameServiceContext.Provider value={gameService}>
      <App/>
    </GameServiceContext.Provider>
  </Provider>
  ,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

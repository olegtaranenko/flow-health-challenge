export const scoresLoaded = (scores) => {
  return {
    type: 'SCORES_LOADED',
    payload: scores
  }
};

export const gameLoaded = (newGame) => {
  return {
    type: 'GAME_LOADED',
    payload: newGame
  }
};

export const gameRequested = () => {
  return {
    type: 'GAME_REQUESTED',
  }
};

export const gameServiceFailed = (error, scope) => {
  return {
    type: 'GAME_QUERY_FAILED',
    failed: error,
    failedScope: scope
  }
};

export const moveRequested = (index) => {
  return {
    type: 'MOVE_REQUESTED',
    index
  }
};

export const moveProceeded = (response) => {
  return {
    type: 'MOVE_PROCEEDED',
    payload: response
  }
};

export const nextGame = (response) => {
  return {
    type: 'NEXT_GAME',
    payload: response,
  }
};

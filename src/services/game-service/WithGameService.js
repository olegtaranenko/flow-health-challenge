import React               from 'react';
import GameServiceContext from "./GameServiceContext";

const WithGameService = () => (Wrapped) => {
  return (props) => {
    return (
      <GameServiceContext.Consumer>
        {
          (GameService) => {
            return <Wrapped {...props} GameService={GameService}/>
          }
        }
      </GameServiceContext.Consumer>
    )
  };
};

export default WithGameService;
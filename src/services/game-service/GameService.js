import { buildAnUrl } from '../../shared-functions'

export default class GameService {

  constructor({ port = 3000 } = {}) {
    // this._apiBase = process.env.API_URL;
    this._apiBase = `http://localhost:${port}/api`
  }

  getUrl = (url, ...params) => {
    return buildAnUrl(url, this._apiBase, ...params);
  };

  async getResource(url, ...params) {
    const response = await fetch(this.getUrl(url, ...params));

    if ( !response.ok ) {
      throw new Error(`Could not fetch ${url}, status: ${response.status}`)
    }

    return await response.json();
  };


  async postResource(url, payload) {

    const response = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: {
        'Content-Type': 'application/json'
      }
    });

    if ( !response.ok ) {
      throw new Error(`Could not post ${url}, status: ${response.status}`)
    }

    return await response.json();
  };

  getCurrentGame = async () => {
    return await this.getResource('game');
  };

  getScores = async () => {
    return await this.getResource('score');
  };

  nextGame = async () => {
    return await this.getResource('game/next');
  };

  doMove = async (index) => {
    const url = this.getUrl('game/move');
    const payload = { index };
    return await this.postResource(url, payload);
  }
}

import React, { Fragment, useEffect } from 'react';
import './App.scss';

import Dashboard from "./components/dashboard";
import Hero from "./components/hero";


function App() {
  useEffect(() => {
    document.title = "Oleg Taranenko - Tic-tac-toe Challenge"
  }, []);

  return <Fragment>
    <Hero/>
    <Dashboard/>
  </Fragment>
}

export default App;
